import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import HomeView from './src/components/HomeView'
import ProviderDetailsView from './src/components/ProviderDetailsView'

const MainNavigator = createStackNavigator({
  home: { screen: HomeView },
  detailsView: { screen: ProviderDetailsView },
});

const App = createAppContainer(MainNavigator);

export default App;