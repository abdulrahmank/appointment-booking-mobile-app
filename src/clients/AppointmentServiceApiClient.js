export const getProviders = (providerName) => {
    return fetch(`http://192.168.43.35:3000/api/v1/providers?name=${providerName ? providerName : ''}`)
      .then((response) => response.json())
      .then((responseJson) => {
        return responseJson;
      }).catch((error) => {
        console.error(error);
      });
  }

export const getProviderDetails = (providerId, date) => {
  return fetch(`http://192.168.43.35:3000/api/v1/providers/${providerId}?date=${date}`)
    .then((respose) => respose.json())
    .then((responseJson) => {
      return responseJson;
    }).catch((error) => {
        console.error(error);
      });
}

export const requestAppointment = (providerId, slotId, dateString) => {
  return fetch(`http://192.168.43.35:3000/api/v1/requests`, {
    method: 'POST',
    headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
    body: JSON.stringify({
      'providerId': providerId,
      'slotId': slotId,
      'userId': "5e078a6a861628e110378503",
      'date': dateString,
    })
  }).then((respose) => respose.json())
    .then((responseJson) => {
      return responseJson;
    }).catch((error) => {
        console.error(error);
    });
}

export const signin = ({username, password}) => {
  console.log('In signin' + JSON.stringify({username, password}));
  return fetch(`http://192.168.43.35:3000/api/v1/signIn`, {
    method: 'POST',
    headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
    body: JSON.stringify({
      username,
      password
    })
  }).then((response) => response.json())
  .then((responseJson) => {
    console.log(responseJson);
    return responseJson;
  });
}