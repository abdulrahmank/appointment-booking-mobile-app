import React, { Component } from "react";
import { AsyncStorage } from "react-native";
import SignInView from "./SignInView";
import ProviderListView from "./ProviderListView";

export default class HomeView extends Component {
    static navigationOptions = {
        title: 'Providers',
    };
    
    constructor(props) {
        super(props);
        this.state = {
            signedIn: false,
        }
    }
    
    componentDidMount() {
        AsyncStorage.getItem("userId").then((res) => {
            this.setState({
                signedIn: (res != null),
            });
        });
    }

    render() {
        if (this.state.signedIn) {
            return <ProviderListView {...this.props} />
        } else {
            return <SignInView {...this.props} onSignIn = {() => this.setState({signedIn: true})} />
        }
    }
}