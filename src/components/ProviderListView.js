import React, { Component } from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text } from 'react-native';
import { SearchBar, ListItem } from 'react-native-elements'
import { getProviders } from '../clients/AppointmentServiceApiClient';

export default class ProviderListView extends Component {
    static navigationOptions = {
        title: 'Providers',
    };

    constructor(props) {
        super(props);
        this.state = {
            providers: []
        }
    }

    componentDidMount() {
        getProviders().then((providers) => {
            this.setState({
                providers: providers,
            });
        })
    }

    updateSearch(searchTerm) {
        this.setState({searchTerm: searchTerm});
        getProviders(this.state.searchTerm).then((providers) => {
            this.setState({
                providers: providers,
            });
        })
    };

    renderItem(item) {
        return(
        <ListItem
            title={`${item.name.toUpperCase()}`}
            subtitle={<View>
                <Text>{`Location: ${item.location}`}</Text>
                <Text>{`Services: ${item.services.join(',')}`}</Text>
            </View>}
            onPress={this.openProviderDetailView(item)}
        />);
    }

    renderHeader() {
        return (
          <SearchBar
            placeholder="Type Here..."
            lightTheme
            round
            onChangeText={text => this.updateSearch(text)}
            autoCorrect={false}
            value={this.state.searchTerm}
          />
        );
    }

    openProviderDetailView  = (rowData) => () => {
        const {navigate} = this.props.navigation;
        navigate('detailsView', {providerId: rowData.id});
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <FlatList
                    data={this.state.providers}
                    renderItem={({ item }) => this.renderItem(item)}
                    keyExtractor={item => item.id}
                    ListHeaderComponent={this.renderHeader()}
                />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});
