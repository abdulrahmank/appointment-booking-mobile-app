import React, {Component} from 'react';
import { View, Button, AsyncStorage, TextInput, StyleSheet } from 'react-native';
import { signin } from '../clients/AppointmentServiceApiClient';

export default class SignInView extends Component {
    static navigationOptions = {
        title: 'Sign in',
    };

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        }
    }

    authenticate = () => {
        signin({
            username: this.state.username,
            password: this.state.password
        }).then(({userId}) => {
            AsyncStorage.setItem("userId", userId).then(() => {
                this.props.onSignIn()
            });
        }).catch((e) => {
            console.log(e);
            this.setState({
                username: '',
                password: '',
            });
        });
    }

    render() {
        return(
            <View style={style.container}>
                <TextInput
                    editable 
                    placeholder = 'Username'
                    onChangeText = {(username) => {
                        this.setState({username: username})
                    }}
                    value = {this.state.username} />
                <TextInput 
                    editable
                    secureTextEntry={true} 
                    placeholder = 'Password'
                    onChangeText = {(password) => {
                        this.setState({password: password})
                    }}
                    value = {this.state.password} />
                <Button title = 'Sign in' onPress = {this.authenticate}/>
            </View>
        );
    }
}

const style = StyleSheet.create({
    container: {
        padding: 20,
    }
})