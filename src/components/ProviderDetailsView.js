import React, { Component } from "react";
import { Text, View, StyleSheet, FlatList, ToastAndroid, Alert } from 'react-native';
import { Card, ListItem } from 'react-native-elements'
import moment from 'moment';

import { getProviderDetails, requestAppointment } from "../clients/AppointmentServiceApiClient";
import DateTimePicker from '@react-native-community/datetimepicker';

export default class ProviderDetailsView extends Component {
    static navigationOptions = {
        title: 'Provider Details',
    };

    constructor(props) {
        super(props);
        this.state = {
            provider: {},
        }
    }

    bookSlot = (slot) => () => {
        Alert.alert(
            'Confirm booking',
            `Do you want to request booking for the choosen time slot on ${moment(this.state.date).format("DD/MM/YYYY")} (DD/MM/YYYY)?`,
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {text: 'OK', onPress: () => {
                requestAppointment(this.state.provider.id, slot._id, this.state.date.toISOString()).then((response) => {
                    ToastAndroid.show(response, ToastAndroid.LONG);
                });}
            },  
            ],
            {cancelable: false},
          );
    }

    renderItem(rowData) {
        return(
            <ListItem
                title={`Timing: ${rowData.startTime} - ${rowData.endTime}`}
                onPress={this.bookSlot(rowData)} />
        );
    }

    setDate = (event, date) => {
        date = date || new Date();
        this.setState({
          date,
        });
        const {providerId} = this.props.navigation.state.params;
        getProviderDetails(providerId).then((provider) => {
            this.setState({
                provider: provider,
            });
        });
      }

    render() {
        const {provider, date} = this.state;
        if (!date) {
            return (
                <DateTimePicker value={date || new Date()}
                    mode='date'
                    is24Hour={true}
                    display="default"
                    onChange={this.setDate} />
            );
        }
        return (
            <View style={styles.container}>
                <Text style={styles.subtitleView}> {provider.name} </Text>
                <Text style={styles.subtitleView}> {provider.services?.join(', ')} </Text>
                <Text style={styles.subtitleView} > {provider.location} </Text>
                <Text style={styles.subtitleView}> Available slots: </Text>
                <FlatList
                    data={provider.slots}
                    renderItem={({ item }) => this.renderItem(item)}
                    keyExtractor={item => item._id}
                />
              </View>
        );
    }
}

const styles = StyleSheet.create({
    subtitleView: {
      flexDirection: 'row',
      paddingLeft: 10,
      paddingTop: 5
    },
    container: {
        paddingLeft: 10,
        paddingTop: 5
    }
});